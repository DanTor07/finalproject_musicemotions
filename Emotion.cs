﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    class Emotion
    {
        public string Emoción { get; set; }
        public List<MusicInfo> MusicList { get; set; }
    }
}
